// 2D arrays in Rust
// https://stackoverflow.com/questions/13212212/creating-two-dimensional-arrays-in-rust
// https://docs.rs/array2d/0.2.1/array2d/
// https://crates.io/crates/block-grid

use array2d::Array2D;          // 2d grids
use itertools::Itertools;      // permutations
use std::collections::HashSet; // logical set
use rand::random;              // random bool
use std::time::Instant;        // basic timing

fn print_grid(a: &Array2D<bool>) {
  print!("\n");
  for row in a.as_rows() {
    for sq in row{
      if sq {
        print!(" x ");
      } else {
        print!(" - ");
      }   
    }
    print!("\n")
  }
  print!("\n")
}


fn gen_hints(grid: &Array2D<bool>) -> (Vec<Vec<usize>>, Vec<Vec<usize>>){

  // Generate row hints
  let mut row_hints = Vec::new();
  let mut col_hints = Vec::new();

  for r in grid.as_rows() {
    row_hints.push(parse_vec(&r));
  }
  // Generate column hints  
  for c in grid.as_columns() {
    col_hints.push(parse_vec(&c));
  }

  return (row_hints, col_hints);
}


fn parse_vec(v: &Vec<bool>) -> Vec<usize> {
  let l = v.len();
  let mut to_return = Vec::new();
  let mut tally = 0;
  let mut i = 1;
  for sq in v{
    if *sq == true {
      tally += 1;
    }
    // If we're at the end of a group, write the tally
    if *sq == false && tally != 0 {
      to_return.push(tally);
      tally = 0;
    // If we're at the end of the vector, write the tally
    } else if *sq == true && i == l {
      to_return.push(tally);
      tally = 0;
    }
    // Keep track of where we are in the vector
    i += 1;
  }
  return to_return;
}


fn is_solved(grid: &Array2D<bool>,
             row_hints: &Vec<Vec<usize>>,
             col_hints: &Vec<Vec<usize>>
            ) -> bool {
  // Create hints for the grid
  let check_hints = gen_hints(grid);
  // Check whether they are the same as what
  // we want to solve
  return check_hints.0 == *row_hints && check_hints.1 == *col_hints;
}


fn gen_legal_rows(row_hint: &Vec<usize>,
                  length: usize,
                 ) -> Vec<Vec<bool>> {
  
  let num_filled = row_hint.iter().sum();
  let num_empty = length - num_filled;
  let mut base = Vec::new();
  // there's probably a way to populate this
  // Vec without appending like this
  for _i in 0..num_filled {
    base.push(true);
  }
  for _j in 0..num_empty {
    base.push(false);
  } 

  let perms = base.into_iter().permutations(length);
  let mut unique_perms = HashSet::new();
  for perm in perms {
    unique_perms.insert(perm);
  }
  let mut to_return = Vec::new();
  for u in &unique_perms {
    let parsed = parse_vec(&u);
    if parsed == *row_hint {
      let mut to_push = vec![false; length];
      to_push.clone_from_slice(&u[0..]);
      to_return.push(to_push);
    } 
  }
  return to_return; 
} 


fn solve(grid: &Array2D<bool>, 
         row_hints: &Vec<Vec<usize>>,
         col_hints: &Vec<Vec<usize>>,
         row_idx: usize) -> bool {


  let r = grid.num_rows();
  let c = grid.num_columns();
  // If the board is in a solved state, we're done
  // Only check when we're in the final row
  // When r is one more than the last valid row_idx,
  // we know that we just changed the final row in the
  // previous recursion so we need to check the solution
  // in this recursion
  if row_idx == r {
    if is_solved(grid, row_hints, col_hints) {
      println!("Solved!");
      print_grid(&grid);
      return true;
    } else {
      return false;
    }
  }
   
  // Generate legal rows for the current row hint
  let legal_rows = gen_legal_rows(&row_hints[row_idx], c);
  // Check each of the legal rows
  for new_row in &legal_rows {
    let mut old_rows = grid.as_rows();
    old_rows[row_idx].clone_from_slice(&new_row[0..]);
    let new_grid = Array2D::from_rows(&old_rows);
    // If it's solved, we're done
    if solve(&new_grid, row_hints, col_hints, row_idx+1) {
      return true
    }
  }
  // If it's not solved after checking all of the legal
  // rows, backtrack up one row
  return false

}

fn gen_empty_board(size: usize) -> Vec<Vec<bool>>{
  let mut grid = Vec::new();
  for _i in 0..size {
    let mut new_row = Vec::new();
    for _j in 0..size {
      new_row.push(false);
    }
    grid.push(new_row);
  }
  return grid
}

fn gen_random_board(size: usize) -> Vec<Vec<bool>>{
  let mut grid = Vec::new();
  for _i in 0..size {
    let mut new_row = Vec::new();
    for _j in 0..size {
      if random() {
        new_row.push(true);
      } else {
        new_row.push(false);
      }
    }
    grid.push(new_row);
  } 
  return grid
}


fn main() {
  
  //let grid_size = 5;  // Worked
  //let grid_size = 6;  // Worked
  let grid_size = 7;  // Didn't work
  //let grid_size = 10; // Didn't work
  let init_grid = Array2D::from_rows(&gen_empty_board(grid_size));
  let truth_grid = Array2D::from_rows(&gen_random_board(grid_size));
  let hints = gen_hints(&truth_grid);
  

  println!("Truth board:");
  print_grid(&truth_grid);

  println!("Solving the puzzle...");
  let start = Instant::now();
  let _solved = solve(&init_grid, &hints.0, &hints.1, 0);
  println!("solved in {} seconds", start.elapsed().as_secs());
  

}
